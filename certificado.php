<?php
  error_reporting('0');
  use Dompdf\Dompdf;
  require 'vendor/autoload.php';
  $info = array(
    "nombre_dueno"=>"Harold Stiven Restrepo",
    "dir_dueno"=>"Allá, voltee a la derecha y baje hasta el telefono",
    "tel_dueno"=>"312885559",
    "sello"=>"2229",
    "nombre_animal"=>"Tobby",
    "especie"=>"Perro",
    "otra_especie"=>"",
    "rabia"=>"Con rabia",
    "edad_meses"=>"3 meses",
    "edad_anos"=>"2 años",
    "peso"=>"20 - 50 lbs",
    "otra"=>"Bla bla",
    "sexo"=>"Masculino",
    "raza_predominante"=>"Labrador",
    "color_marcas_predominante"=>"Café con blanco, sin manchas",
    "num_microship"=>"12366",
    "num_chapa_rabia"=>"",
    "fecha_vacunacion"=>"Diciembre 01 2018",
    "fecha_vencimiento"=>"Diciembre 01 2022",
    "nombre_producto"=>"Aguapanela",
    "manufacturero"=>"AGP",
    "num_serie_vacuna"=>"99855",
    "fecha_expedicion_vacuna"=>"12/12/2017",
    "nombre_veterinario"=>"Jonh Doe",
    "direccion_veterinario"=>"Clinica veterinaria la 80",
    "firma"=>"http://via.placeholder.com/300x300",
    "num_licencia"=>"626262"
  );

  $myHTML ='<!doctype html>
            <html lang="es">
              <head>
                <meta charset="UTF-8">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
                <title>Certificado</title>
              </head>
              <body>
                <div class="container">
                    <div class="row">            
                        <div class="col-xs-1">
                            <img src="http://via.placeholder.com/300x300" class="img-responsive">
                        </div>
                        <div class="col-xs-8">
                            <center>CERTIFICADO OFICIAL DE VACUNACION DEL COLEGIO DE MEDICOS VETERINARIOS DE PUERTO RICO</center>
                        </div>
                        <div class="col-xs-1">
                            <h2><code>409847</code></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-10">
                            <center>Toda vacunación contra la Rabia llevará este certificado</center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <b><u>Información del Dueño:</u></b><br>
                            <b>Nombre:</b> '.$info["nombre_dueno"].'<br>
                            <b>Dirección:</b> '.$info["dir_dueno"].' <br>
                            <b>Tel:</b>  '.$info["tel_dueno"].'
                        </div>
                        <div class="col-xs-5">
                            <br>
                            # SELLO: '.$info["sello"].' 
                            <img src="http://via.placeholder.com/300x300" class="img-responsive" width="100" >    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5">
                            <b><u>Nombre Animal:</u></b> '.$info["nombre_animal"].' <br>
                            <b><u>Especie</u> </b> '.$info["especie"].'<br>
                            <u>'.$info["otra_especie"].'</u><br>
                            <br>
                            <b><u>Rabia</u></b> '.$info["rabia"].'<br>
                            <br>
                            <b><u>Otra</u></b><br>
                            '.$info["otra"].'<br>
                            <br>
                            <b><u>Edad Meses:</u></b> '.$info["edad_meses"].'<br>
                            <b><u>Edad Años:</u></b> '.$info["edad_anos"].' <br>
                            <br>
                            <b><u>Peso</u> </b> '.$info["peso"].'<br>
                            <br>
                            <b><u>Sexo</u></b> '.$info["sexo"].'<br>
                            <br>            
                            <b><u>Raza predominante</u> </b> <br>
                            '.$info["raza_predominante"].'<br>
                            <b><u>Color y Marcas Predominantes</u></b> <br>
                            '.$info["color_marcas_predominante"].'  
                        </div>          
                        <div class="col-xs-5">
                            <b><u>Microship #</u></b> '.$info["num_microship"].'<br>
                            <b><u>Chapa Rabia #</u></b> '.$info["num_chapa_rabia"].'<br>
                                    <b><u>Fecha de Vacunación</u></b><br>
                                    <center>
                                    '.$info["fecha_vacunacion"].'
                                    <br>
                                    <small class="text-muted text-center">mes/día/año</small>
                                    </center>
                                    <b><u>Fecha de Vencimiento</u></b><br>
                                    <center>
                                    '.$info["fecha_vencimiento"].'
                                    <br>
                                    <small class="text-muted text-center">mes/día/año</small>
                                    </center>                  
                                        <b><u>Nombre del Producto</u></b><br>
                                        '.$info["nombre_producto"].'<br>
                                        <b><u>Manufacturero <small>(primeras 3 letras)</small></u></b><br>
                                        '.$info["manufacturero"].'<br>
                                        <b><u># Serie de Vacuna (lote)</u></b> <br>
                                        '.$info["num_serie_vacuna"].'<br>
                                        <b><u>Fecha de Expedicion de Vacuna</u></b><br>
                                        '.$info["fecha_expedicion_vacuna"].'<br>
                                        <br>                    
                                <b><u>Información del Veterinario</u></b><br>
                                <br>
                                <b><u>Nombre:</u></b><br>
                                '.$info["nombre_veterinario"].'<br>
                                <b><u>Dirección:</u></b><br>
                                '.$info["direccion_veterinario"].'<br>
                                <b><u>Firma:</u></b><br>
                                <center><img src="'.$info["firma"].'" class="img-responsive"></center><br>
                                <b><u># Licencia:</u></b> '.$info["num_licencia"].'
                        </div>
                    </div>
                </div>
              </body>
            </html>';

    $dompdf = new Dompdf();
    $dompdf->set_option('isRemoteEnabled', TRUE);
    $dompdf->loadHtml($myHTML);
    $dompdf->setPaper('A4', 'portrait');
    // Render the HTML as PDF
    $dompdf->render();
    $dompdf->stream("Certificado.pdf");  
?>